package com.example.recyclerviewaddingword.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewaddingword.adapter.DisplayAdapter
import com.example.recyclerviewaddingword.databinding.FragmentDisplayBinding

class DisplayFragment : Fragment() {

    private var _binding: FragmentDisplayBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DisplayFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDisplayBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val individualWords = args.string
        val singleWord = individualWords.split(" ").toList()
        with(binding) {
            rvDisplay.layoutManager = LinearLayoutManager(context)
            rvDisplay.adapter = DisplayAdapter().apply {
                addIndividualWords(singleWord)
            }
            btnBack.setOnClickListener {
                val backDirection = DisplayFragmentDirections.actionDisplayFragmentToWordsFragment()
                findNavController().navigate(backDirection)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}