package com.example.recyclerviewaddingword.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewaddingword.adapter.WordAdapter
import com.example.recyclerviewaddingword.databinding.FragmentWordsBinding
import com.example.recyclerviewaddingword.viewmodel.WordViewModel

class WordsFragment : Fragment() {

    private var _binding: FragmentWordsBinding? = null
    private val binding get() = _binding!!
    private val wordViewModel by viewModels<WordViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentWordsBinding.inflate(
        inflater,
        container,
        false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            btnDisplay.setOnClickListener {

                val input = etWord.text.toString()
                wordViewModel.addToList(input)
                rvWords.layoutManager = LinearLayoutManager(context)
                rvWords.adapter = WordAdapter().apply {
                    wordViewModel.getWords()
                    wordViewModel.word.observe(viewLifecycleOwner) {
                        addWords(it)
                    }
                }
                etWord.text.clear()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}