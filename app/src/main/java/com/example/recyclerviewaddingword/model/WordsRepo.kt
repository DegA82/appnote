package com.example.recyclerviewaddingword.model

import com.example.recyclerviewaddingword.model.remote.WordApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object WordsRepo {
    //Initalized
    private val wordApi = object : WordApi {
        override suspend fun getWords(): List<String> {
            return listOf()
        }
    }

    suspend fun getWords(): List<String> = withContext(Dispatchers.IO) {
        wordApi.getWords()
    }
}