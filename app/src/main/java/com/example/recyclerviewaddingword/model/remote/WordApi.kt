package com.example.recyclerviewaddingword.model.remote

interface WordApi {

    suspend fun getWords(): List<String>
}