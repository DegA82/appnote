package com.example.recyclerviewaddingword.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.recyclerviewaddingword.model.WordsRepo
import kotlinx.coroutines.launch

class WordViewModel : ViewModel() {
    //need instance of repo
    private val repo = WordsRepo
    private val temList = mutableListOf<String>()

    private val _words = MutableLiveData<List<String>>()
    val word: LiveData<List<String>> get() = _words

    fun getWords() {
        viewModelScope.launch {
            val itemList = temList
            _words.value = itemList
        }
    }

    fun addToList(input: String) {
        temList.add(input)
    }
}