package com.example.recyclerviewaddingword.viewmodel

data class WordState(

    val _isLoading : Boolean = false,
    val wordList: List<String> = emptyList()
)
