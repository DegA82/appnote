package com.example.recyclerviewaddingword.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewaddingword.databinding.ItemWordBinding
import com.example.recyclerviewaddingword.view.WordsFragmentDirections

class WordAdapter : RecyclerView.Adapter<WordAdapter.WordViewHolder>() {

    private var words = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val binding = ItemWordBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return WordViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val word = words[position]
        holder.bindWord(word)
    }

    override fun getItemCount(): Int {
        return words.size
    }

    fun addWords(words: List<String>) {
        this.words = words.toMutableList()
        notifyDataSetChanged()
    }

    class WordViewHolder(
        private val binding: ItemWordBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindWord(word: String) {
            binding.tvList.text = word
            binding.tvList.setOnClickListener {
                it.findNavController()
                    .navigate(WordsFragmentDirections.actionWordsFragmentToDisplayFragment(word))
            }
        }
    }
}
