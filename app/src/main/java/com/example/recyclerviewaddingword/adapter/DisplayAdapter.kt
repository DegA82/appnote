package com.example.recyclerviewaddingword.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewaddingword.databinding.ItemDisplayBinding

class DisplayAdapter : RecyclerView.Adapter<DisplayAdapter.DisplayViewHandler>() {

    private var individualWords = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisplayViewHandler {

        val binding = ItemDisplayBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DisplayViewHandler(binding)
    }

    override fun onBindViewHolder(holder: DisplayViewHandler, position: Int) {
        val individualWord = individualWords[position]
        holder.bindDisplay(individualWord)
    }

    override fun getItemCount(): Int {
        return individualWords.size
    }

    fun addIndividualWords(words: List<String>) {
        this.individualWords = words.toMutableList()
//        Log.d("LOGGER", individualWords.toString() + " " + words.toString())
        notifyDataSetChanged()
    }

    class DisplayViewHandler(
        private val binding: ItemDisplayBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDisplay(word: String) {
            binding.tvIndividualWord.text = word
        }
    }
}